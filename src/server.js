const express = require("express");
const indexRouter = require('./routes/index.routes.js');

const app = express();

app.use(express.json());

app.use('/index', indexRouter);

app.get('', (req, res) =>{
   res.send("<h1>Un Saludo a todos!!</h1>");
});

app.listen(3000, () => {
    console.log('Server is listeing on port 3000');
});