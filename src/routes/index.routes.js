const { Router } = require('express');

const router = Router();

router.get('/', (req, res) => {
    res.send("<h1>Un saludo a todos desde routers</h1>")
});

router.post('/:id', (req, res) => {
    res.send({id: req.params.id, ...req.body});
})

module.exports = router;